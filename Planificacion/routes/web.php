<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Docentes
Route::get('/Docentes', 'DocenteController@index')->name('docentes');
Route::post('/Docentes/Crear', 'DocenteController@onCreateDocente')->name('docentes.create');
Route::post('/Docentes/All', 'DocenteController@onAllDocentes')->name('docentes.all');
Route::post('/Docentes/Eliminar', 'DocenteController@onDeleteDocente')->name('docentes.delete');
Route::post('/Docentes/Search', 'DocenteController@onSearchDocente')->name('docentes.search');
Route::post('/Docentes/Actualizar', 'DocenteController@onUpdateDocente')->name('docentes.update');
Route::get('/Docentes/lista-completa-docentes', 'DocenteController@onlist')->name('docentes.list');
Route::post('/Docentes', 'DocenteController@upgradeDocentes')->name('docentes.upgrade');
Route::get('/Docentes/progreso-de-los-docentes', 'DocenteController@onProgreso')->name('docentes.progress');
Route::post('/Docentes/progreso', 'DocenteController@onViewProgreso')->name('docentes.progreso.view');
Route::post('/Docentes/progreso/eliminar-grupo', 'DocenteController@onDeleteProgresoGrup')->name('docentes.progreso.deleteGrup');
Route::get('/Docentes/carga-academica', 'DocenteController@cargaAcademica')->name('docentes.cargaAcademica');
Route::get('/Docentes/carga-academica-sin-carga-horaria', 'DocenteController@cargaAcademicaSinCargaHoraria')->name('docentes.cargaAcademicaSinCargaHoraria');
Route::get('/Docentes/carga-academica-horaria', 'DocenteController@cargaAcademicaHoraria')->name('docentes.cargaAcademicaHoraria');
Route::get('/Docentes/carga-academica-generica', 'DocenteController@cargaAcademicaGenerica')->name('docentes.cargaAcademicaGenerica');

//Componentes
Route::get('/Componentes', 'ComponenteController@index')->name('componentes');
Route::post('/Componentes/Crear', 'ComponenteController@onCrearComponente')->name('componentes.create');
Route::post('/Componentes/Search', 'ComponenteController@onSearchComponente')->name('componentes.search');
Route::post('/Componentes/Actualizar', 'ComponenteController@onUpdateComponente')->name('componentes.update');
Route::post('/Componentes/Eliminar', 'ComponenteController@onDeleteComponente')->name('componentes.delete');
Route::post('/Componentes', 'ComponenteController@onSearchComp')->name('componentes.searchAtOne');
Route::post('/Componentes/horas', 'ComponenteController@updateHoursComp')->name('componentes.HoursComp');
Route::post('/Componentes/docentes', 'ComponenteController@ongetDocentes')->name('componentes.getDocentes');
Route::get('/Componentes/IS', 'ComponenteController@IS')->name('componentes.IS');
Route::get('/Componentes/IT', 'ComponenteController@IT')->name('componentes.IT');
Route::get('/Componentes/SE', 'ComponenteController@SE')->name('componentes.SE');
Route::get('/Componentes/UALN', 'ComponenteController@UALN')->name('componentes.UALN');
Route::get('/Componentes/carga-academica-componentes', 'ComponenteController@cargaAcademica')->name('componentes.cargaAcademica');

//Horario
Route::get('/Horarios', 'HorarioController@index')->name('horarios');
Route::get('/Horarios/crear-horarios', 'HorarioController@onGetlist')->name('horario.crear');
Route::post('/Horarios/crear', 'HorarioController@onSetlist')->name('horario.edit');
Route::post('/Horarios/crear-un-horario', 'HorarioController@onSetScheduleClassRoom')->name('horario.start');
Route::get('/Horarios/docentes', 'HorarioController@onDocenteHr')->name('horario.docente');
Route::post('/Horarios/docentes/View', 'HorarioController@onDocenteHrView')->name('horario.templadeD');
Route::get('/Horarios/año', 'HorarioController@onAnyoHr')->name('horario.anyo');
Route::post('/Horarios/año', 'HorarioController@onAnyoHrView')->name('horario.viewA');
Route::get('/Horarios/aula', 'HorarioController@onAulahr')->name('horario.aula');
Route::post('/Horarios/docentes', 'HorarioController@onAulaHrView')->name('horario.viewU'); 

//Aula
Route::get('/Aulas', 'AulaController@index')->name('aulas');
Route::post('/Aulas/Crear', 'AulaController@onCrearAula')->name('aulas.create');
Route::post('/Aulas/Search', 'AulaController@onSearchAula')->name('aulas.search');
Route::post('/Aulas/Actualizar', 'AulaController@onUpdateAula')->name('aulas.update');
Route::post('/Aulas/Eliminar', 'AulaController@onDeleteAula')->name('aulas.delete');

//Anyo
Route::get('/Años', 'AnyoController@index')->name('anyos');
Route::post('/Años/All', 'AnyoController@onAllYear')->name('anyos.all');
Route::post('/Años/Save', 'AnyoController@onSaveYear')->name('anyos.save');
Route::post('/Años/crear-un-año-escolar', 'AnyoController@onCreateYear')->name('anyos.create');

//Grupo
Route::get('/Grupos', 'GrupoController@index')->name('grupos');
Route::post('/Grupos/Crear', 'GrupoController@onCreateGrup')->name('grupos.create');
Route::post('/Grupos/Actualizar', 'GrupoController@onUpgradeGrup')->name('grupos.upgrade');
Route::post('/Grupos/Eliminar', 'GrupoController@onDeleteGrup')->name('grupos.delete');

//API
Route::group(array('prefix' => 'api/v2'), function()
{
    //Ciclo Académico
    Route::get('cycles/all','ApiController@onGetCyclesAll');
    //Horarios
    Route::get('schedules/all','ApiController@onGetSchedulesAll');
    //Docentes
    Route::get('teachers/all','ApiController@onGetTeachersAll');
    Route::get('teachers/schedule/{cycle}/{id}','ApiController@onGetTeacherSchedule');
    //Aula
    Route::get('classrooms/all','ApiController@onGetClassroomsAll');
    Route::get('classrooms/schedule/{cycle}/{id}','ApiController@onGetClassroomSchedule');
    //Anyo
    Route::get('anyos/schedule/{cycle}/{profession}/{id}','ApiController@onGetProfessionAnyosSchedule');
});