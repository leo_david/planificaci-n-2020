@extends('layouts.app')

@section('content')
<div class="container">
    <center>
      <h4>Universidad Nacional Autónoma de Nicaragua</h4>
      <h5>UNAN - León</h5>
      <h6>Departamento de Computación</h6>
      <h6>Reporte de Carga Horaria Genérica de Docencia Directa {{Session::get('IdSem')}} {{Session::get('IdName')}}</h6>
    </center>
    <hr />
    <div class="row">
        <div class="col-md-12">
          <table class="table m-0">
            <thead>
              <tr class="thead bg-light">
                <th>Departamento</th>
                <th>Carga horaria</th>
                <th class="pl-0">Carrera</th>
                <th class="pl-0">TGrupo</th>
                <th class="pl-0">THoras</th>
              </tr>
            </thead>
            @php
                // Iniciamos la creación de las filas de la tabla
                $filas = count($collection);
                if (count($collection) != 0) {
                    $iddocente = $collection[0]->iddocente;
                }
                $dochorario = 1;
                $Grantotalgrupos = 0;
                $Grantotalhoras = 0;
            @endphp
            <tbody>
                @if ($filas != 0)
                    @for ($i=0 ; $i < $filas; $i++)
                    @if ($iddocente != $collection[$i]->iddocente)
                        @php
                            $iddocente = $collection[$i]->iddocente;
                            $dochorario++;
                        @endphp
                    @endif
                    @php
                        // Seleccionamos el Id actual para saber si es el mismo componente
                        $idcomponente = $collection[$i]->idcomponente;      
                        $componente = $collection[$i]->componente;
                        $modalidad = $collection[$i]->modalidad;
                        $nombrecarrera = $collection[$i]->nombrecarrera;
                        $totalgrupos = 0;
                        $totalhoras = 0;
                    @endphp
                    @foreach ($collection as $item)
                        @if ($item->idcomponente == $idcomponente)
                            @php
                                $totalgrupos++;
                                $totalhoras = $totalhoras + $item->horasgrupo;
                            @endphp
                        @endif
                    @endforeach
                    @php
                        $Grantotalgrupos = $Grantotalgrupos + $totalgrupos;
                        $Grantotalhoras = $Grantotalhoras + $totalhoras;
                        echo "<tr>";
                        echo "<td>Computación</td>";
                        echo "<td width='40%' class='p-1'>HORARIO 0" . $dochorario . " - " . $componente;

                        if($collection[$i]->modalidad == 'VIRTUAL')
                            echo " <span class='text-danger font-weight-bold'>[" . $modalidad . "]</span>";
                            echo "</td>";
                            echo "<td class='p-1'>" . $nombrecarrera . "</td>";
                            echo "<td class='p-1'>" . $totalgrupos . "</td>";
                            echo "<td class='p-1'>" . $totalhoras . "</td>";
                            echo "</tr>";
                    @endphp
                @endfor
                @php
                    echo "<tr><td></td><td></td><td></td>";
                    echo "<td class='p-1'><b>" . $Grantotalgrupos . "</b></td><td class='p-1'><b>" . $Grantotalhoras . "</b></td></tr>";
                @endphp
                @endif
            </tbody>
          </table>
        </div>
    </div>
</div>
<br />
<br />
@endsection