<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Grupo extends Model
{
    //Buscar grupos por id de componete
    public static function findByComponent($id){
        $list = DB::table('grupos')->where('idcomponente','=',$id)
        ->orderByDesc('tipo')->get();
        return $list;
    }

    // Listar grupos no asignados
    public static function listAllGruposNoAsignados($idanyos){
        $list = DB::table('grupos')->select('grupos.id as id','idcomponente','componente','tipo',
        'numero','grupos.horas as horasgrupo','nombrecarrera')
        ->join('componentes','grupos.idcomponente','=','componentes.id')
        ->where([
            ['asignar','=',1],
            ['idanyos','=',$idanyos]
        ])
        ->orderBy('idcomponente')
        ->get();
        return $list;
    }

    //Listar todos los grupos que deben planificarsele horario
    public static function listAllGruposParaHorario(){
        $list = DB::table('grupos')->select('grupos.id as idgrupo','carrera', 'anyo', 'idcomponente', 'componente', 
        'tipo', 'numero', 'grupos.horas as horasgrupo', 'iddocente', 'docentes.nombre as docentenombre')
        ->join('componentes','grupos.idcomponente','=','componentes.id')
        ->join('docentes','docentes.id','=','grupos.iddocente')
        ->whereIn('carrera',['IS', 'IT', 'SEI'])
        ->orderBy('carrera')
        ->orderBy('idcomponente')
        ->get();
        return $list;
    }
}
